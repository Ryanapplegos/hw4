#include "pairs.h"
#include <iostream>

using namespace std;

int main(int argc, char* argv[]){
  if(argc != 2){
    cout << "Incorrect argument count" << endl;
  }
  vector<match> words;
  string command = argv[1];
  pull_text(words);
  sortcomm(command, words);
  return 1;
}
