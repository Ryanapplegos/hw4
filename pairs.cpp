#include "pairs.h"
#include <iostream>
#include <map>
#include <algorithm>
#include <cstdlib>

using namespace std;

void pull_text(vector<match>& words){
  string current_word;
  string last_word = "<START>"; // accounts for first case

  while(cin >> current_word){
    bool met = false;
    match these(last_word, current_word);
    vector<match>::iterator it = words.begin();

    // checks whether match is already in function
    while(it < words.end()){
      if(last_word == it->get_first() && current_word == it->get_second()){
        it->one_more();
        met = true;
        break;
      }
      it++;
    }

    // if match not found, add to end
    if(met == false){
      words.push_back(these);
    }
    last_word = current_word;
  }

  // manually adjust for final pairing
  match last(last_word, "<END>");
  words.push_back(last);
}

//Defines match class comparison for vector.sort()
bool compare_a(const match& lhs, const match& rhs){
  return ((lhs.get_first() < rhs.get_first()) || (lhs.get_first() == rhs.get_first() && lhs.get_second() < rhs.get_second()));
}
bool compare_r(const match& lhs, const match& rhs){
  return ((lhs.get_first() > rhs.get_first()) || (lhs.get_first() == rhs.get_first() && lhs.get_second() > rhs.get_second()));
}
bool compare_c(const match& lhs, const match& rhs){
  if(lhs.get_occ() != rhs.get_occ()){
    return (lhs.get_occ() < rhs.get_occ());
  }
  else{
    return ((lhs.get_first() < rhs.get_first()) || (lhs.get_first() == rhs.get_first() && lhs.get_second() < rhs.get_second()));
  }
}

//sorts vector "unsorted" by command
void sortcomm(string command, vector<match>& unsorted){
  bool sorted = false;

  if(command == "a"){ // alphabetically
    sort(unsorted.begin(), unsorted.end(), compare_a);
    sorted = true;
  }
  else if(command == "r"){ // reverse alphabetically
    sort(unsorted.begin(), unsorted.end(), compare_r);
    sorted = true;
  }
  else if(command == "c"){ //  order of occurance
    sort(unsorted.begin(), unsorted.end(), compare_c);
    sorted = true;
  }
  else{
    cout << "Invalid command" << endl;
  }

  if(sorted){
    vector<match>::iterator it = unsorted.begin();
    while(it < unsorted.end()){
      cout << it->get_first() << " " << it->get_second() << " " << it->get_occ() << endl;
      it++;
    }
  }
}

//passes word and vector of match objects, returns randomly generates following word
string getnext(const string word, const vector<match> pairs){
  map<int, string> chance;
  int size = 0;

  for(vector<match>::const_iterator it = pairs.begin(); it != pairs.end(); it++){
    if(word == it->get_first()){
      int sizec = size; //holds size of map list before new pairs are added
      for(int i = sizec + 1; i <= it->get_occ() + sizec; i++){
        chance[i] = it->get_second();
        size++;
      }
    }
  }

  return chance[1 + rand() % size];
}

