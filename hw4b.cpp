#include "pairs.h"
#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[]){
  if(argc != 2){
    cout << "Incorrect argument count" << endl;
  }
  vector<match> words;
  string curr;
  int count = atoi(argv[1]);
  srand(4056);
  pull_text(words);
  for(int i = 0; i < count; i++){
    curr = "<START>";
    while(curr != "<END>"){
      cout << curr << " ";
      curr = getnext(curr, words);
    }
    cout << curr << endl;
  }
  return 1;
}
