CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

hw4a: hw4a.o pairs.o
	g++ -o hw4a hw4a.o pairs.o

hw4b: hw4b.o pairs.o
	g++ -o hw4b hw4b.o pairs.o

test_hw4: test_hw4.o pairs.o
	g++ -o test_hw4 test_hw4.o pairs.o

hw4a.o: hw4a.cpp pairs.h
	g++ -c hw4a.cpp $(CFLAGS)

hw4b.o: hw4b.cpp pairs.h
	g++ -c hw4b.cpp $(CFLAGS)

test_hw4.o: test_hw4.cpp pairs.h
	g++ -c test_hw4.cpp $(CFLAGS)

pairs.o: pairs.cpp pairs.h
	g++ -c pairs.cpp $(CFLAGS)

.PHONY: clean
clean:
	rm -f *.o main?
