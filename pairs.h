#ifndef PAIRS_H
#define PAIRS_H

#include <string>
#include <vector>

class match{
public:

match() : first(), second(), occ(1) {}
match(std::string f, std::string s) : first(f), second(s), occ(1) {}

void store_first(std::string in){first = in;}

std::string get_first(void) const{return first;}

void store_second(std::string in){second = in;}

std::string get_second(void) const{return second;}

int get_occ(void) const{return occ;}

void one_more(void){occ++;}

private:
  std::string first;
  std::string second;
  int occ;
};

//pulls text from cin
void pull_text(std::vector<match>&);

//Sorts on appropriate command, prints error otherwise
void sortcomm(std::string, std::vector<match>&);

std::string getnext(const std::string, const std::vector<match>);

#endif
