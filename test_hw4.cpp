#include "pairs.h"
#include <iostream>
#include <cstdlib>

using namespace std;

//tests functions in pairs.cpp, prints status of match vector after every sort
int main(void){
  vector<match> net;
  string curr = "<START>";
  srand(5550);
  pull_text(net);

  cout << "Unsorted:" << endl;
  for(vector<match>::iterator it = net.begin(); it != net.end(); it++){
    cout << it->get_first() << " " << it->get_second() << " " << it->get_occ() << endl;
  }

  cout << endl << "Alphabetical:" << endl;
  sortcomm("a", net);
  cout << endl << "Reverse alpah:" << endl;
  sortcomm("r", net);
  cout << endl << "By count:" << endl;
  sortcomm("c", net);
  cout << endl << "Testing an invalid input:" << endl;
  sortcomm("wrong", net);

  cout << endl << "5 Randomly generated texts:" << endl;
  for(int i = 0; i < 5; i++){
    curr = "<START>";
    while(curr != "<END>"){
      cout << curr << " ";
      curr = getnext(curr, net);
    }
    cout << curr << endl;
  }
}
