Morgan Hobson    mhobson5  4/3/2016
Homework 4

hw4a
one command line argument is accepted as "a" "r" or "c" for pair ordering. The text is then taken through cin.
Thus a valid command may be "./hw4a a < he_thought.txt"

hw4b
one command line argument is accepted as an int, which will be the number of random texts generated. The text is then taken through cin.
Thus a valid command may be "./hw4b 10 < he_thought.txt"

test_hw4
no command line arguments. Accepts input through cin.
Thus a valid command may be "./test_hw4 < he_thought.txt"
Almost all implementation is in pairs.cpp, thus testing here is just displaying the results of functions in pairs.cpp from a given text file.
Functions were tested against a "normal" text file, a blank text file, a text file with one word, a text file with many spaces, and a text file with many repeating words. Invalid commands were also tested.
It should be noted that the getnext funtion in pairs.cpp doesn't generate an entire text, it only generates the next word based on the current word.
This means that random text generation depends on main to cycle getnext until <END> is reached. This is the only real implementation outside of pairs.cpp.
The method of cycling in test_hw4 is copied directly from hw4b.
